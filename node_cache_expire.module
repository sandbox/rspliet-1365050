<?php
/**
 * node_cache_expire.module
 * December 8th, 2011
 * Copyright (C) 2011 Roy Spliet, Sapito B.V.
 *
 * Main module facilitating node cache expiration.
 * Overrides the default caching mechanics for the cache_field bin
 * to set the expiry field and adhere to the contents of it.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * NodeCacheExpireFieldDatabaseCache
 * Implements a cache object that plays "by the rules" of the expiry field
 * @extends DrupalDatabaseCache
 */
class NodeCacheExpireFieldDatabaseCache extends DrupalDatabaseCache {
	function set($cid, $data, $expire = CACHE_PERMANENT) {
		$keys = explode(":",$cid);
		if(count($keys) > 2 && $keys[1] == "node") {
			$type = _node_cache_expire_type_from_nid($keys[2]);
			if($type === FALSE) {
				parent::set($cid,$data,$expire);
				return;
			}
			
			$expire = _node_cache_expire_get($type);
			if(intval($expire) > 0) {
				parent::set($cid,$data,time()+intval($expire));
			} else {
				parent::set($cid,$data,$expire);
			}
		} else {
			parent::set($cid,$data,$expire);
		}
	}
	
	function getMultiple(&$cids) {
		try {
			// Garbage collection necessary when enforcing a minimum cache lifetime.
			$this->garbageCollection($this->bin);

			// When serving cached pages, the overhead of using db_select() was found
			// to add around 30% overhead to the request. Since $this->bin is a
			// variable, this means the call to db_query() here uses a concatenated
			// string. This is highly discouraged under any other circumstances, and
			// is used here only due to the performance overhead we would incur
			// otherwise. When serving an uncached page, the overhead of using
			// db_select() is a much smaller proportion of the request.
			$result = db_query('SELECT cid, data, created, expire, serialized FROM {' . db_escape_table($this->bin) . '} WHERE (expire > :now OR expire <= 0) AND cid IN (:cids)', array(':cids' => $cids,':now' => time()));
			$cache = array();
			foreach ($result as $item) {
				$item = $this->prepareItem($item);
				if ($item) {
					$cache[$item->cid] = $item;
				}
			}
			$cids = array_diff($cids, array_keys($cache));
			return $cache;
		}
		catch (Exception $e) {
			// If the database is never going to be available, cache requests should
			// return FALSE in order to allow exception handling to occur.
			return array();
		}
	}
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Adds menu options to the node type form.
 */
function node_cache_expire_form_node_type_form_alter(&$form, $form_state) {
	$type = $form['#node_type'];
	$form['cache'] = array(
		'#type' => 'fieldset',
		'#title' => t('Cache settings'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
		'#group' => 'additional_settings',
	);
	
	$options = array(
		-1 => "On next cache clear",
		0 => "Never",
		3600 => "One hour",
		7200 => "Two hours",
		10800 => "Three hours",
		21600 => "Six hours",
		43200 => "Twelve hours",
		86400 => "One day",
		604800 => "One week"
	);
	$form['cache']['cache_expire'] = array(
		'#type' => 'select',
		'#title' => t('Cache expires'),
		'#default_value' => _node_cache_expire_get($type->type),
		'#options' => $options,
		'#description' => t('Choose the cache expiration duration for this node type. Always calculated from generation time (page view).'),
		'#attributes' => array('class' => array('menu-title-select')),
	);
	
	$form['#submit'][] = 'node_cache_expire_submit';
}

/**
 * form_node_type_form submit handler
 *
 * Replaces the default cache expiry time in our database */
function node_cache_expire_submit($form, &$form_state) {
	$type = $form_state['values']['type'];
	$exp = 0;
	if(array_key_exists('cache_expire',$form_state['input']))
		$exp = intval($form_state['input']['cache_expire']);
		
	_node_cache_expire_set($type,$exp);
}

/**
 * Implement hook_form_FORM_ID_alter()
 *
 * Add our hook when deleting a node type
 */
function node_cache_expire_form_node_type_delete_confirm_alter(&$form, $form_state) {
	$form['#submit'][] = 'node_cache_expire_submit_delete';
}

/**
 * form_node_type_delete_confirm submit handler
 *
 * Deletes the default cache expiry time in our database */
function node_cache_expire_submit_delete($form, &$form_state) {
	$type = $form_state['values']['type'];
	_node_cache_expire_delete($type);
}

/**
 * For given node id, retreive the nodes technical type name
 * You cannot use node_load() here as it would result in an endless loop
 */
function _node_cache_expire_type_from_nid($pNid) {
	$result = db_query("SELECT type FROM {node} WHERE nid = :nid",array('nid' => $pNid));
	if($result->rowCount() > 0) {
		$record = $result->fetchAssoc();
		return $record['type'];
	}
	
	return FALSE;
}

/**
 * Set the node type expiry time
 */
function _node_cache_expire_set($pType, $pValue) {
	db_query("REPLACE INTO {node_cache_expire} SET node_type = :type, expire = :expire",array('type' => $pType, 'expire' => $pValue));
}

/**
 * Delete the node type expiry time
 */
function _node_cache_expire_delete($pType) {
	db_delete('node_cache_expire')->condition('node_type',$pType)->execute();
}

/**
 * Get the expiry times for node types
 */
function _node_cache_expire_get($pType) {
	static $expire;
	
	if(!is_array($expire)) $expire = array();
	if(array_key_exists($pType,$expire)) return $expire[$pType];
	
	$result = db_query("SELECT node_type,expire FROM {node_cache_expire} WHERE node_type = :type",array('type' => $pType));
	if($result->rowCount() > 0) {
		$record = $result->fetchAssoc();
		$expire[$pType] = $record['expire'];
		return $expire[$pType];
	}
	
	return 0;
}